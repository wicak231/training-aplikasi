-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 28 Feb 2023 pada 05.15
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `appmedic`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_jenis_obat`
--

CREATE TABLE `tb_jenis_obat` (
  `id_jenis_obat` int(4) NOT NULL,
  `nama_jenis_obat` varchar(100) NOT NULL,
  `created_at` varchar(60) NOT NULL,
  `updated_at` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_jenis_obat`
--

INSERT INTO `tb_jenis_obat` (`id_jenis_obat`, `nama_jenis_obat`, `created_at`, `updated_at`) VALUES
(1, 'Obat Flu', '', '2023-02-24 08:14:48'),
(2, 'Obat Sakit Kepala\r\n', '', ''),
(20, 'Obat Suket', '2023-02-23 02:10:49', '2023-02-23 02:10:58'),
(21, 'Obat Batuk', '2023-02-24 06:31:10', '2023-02-24 08:18:56'),
(22, 'Obat 2', '', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_obat`
--

CREATE TABLE `tb_obat` (
  `id_obat` int(4) NOT NULL,
  `id_jenis_obat` int(4) NOT NULL,
  `nama_obat` varchar(100) NOT NULL,
  `satuan` varchar(100) NOT NULL,
  `harga` varchar(100) NOT NULL,
  `stok` int(4) NOT NULL,
  `tanggal_expired` varchar(100) NOT NULL,
  `created_at` varchar(100) NOT NULL,
  `updated_at` varchar(100) NOT NULL,
  `gambar` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_obat`
--

INSERT INTO `tb_obat` (`id_obat`, `id_jenis_obat`, `nama_obat`, `satuan`, `harga`, `stok`, `tanggal_expired`, `created_at`, `updated_at`, `gambar`) VALUES
(12, 20, 'Antigulma', 'ml', '600000', 3, '2023-02-01', '2023-02-23 03:42:24', '2023-02-23 03:52:03', ''),
(13, 20, 'Gramason', 'ml', '600000', 30, '2023-03-11', '2023-02-23 03:53:13', '2023-02-23 03:53:13', ''),
(14, 2, 'Insto wwww', 'ml', '600000', 15, '2023-02-25', '2023-02-24 06:34:26', '2023-02-24 08:13:54', ''),
(15, 2, 'Ambroxol', 'mg', '2000', 5, '2023-02-01', '2023-02-24 06:34:59', '2023-02-24 06:34:59', ''),
(16, 2, 'Komix', 'mg', '2000', 10, '2023-03-11', '2023-02-24 06:35:21', '2023-02-24 06:35:21', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(3) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(10) NOT NULL,
  `fullname` varchar(100) NOT NULL,
  `is_active` varchar(10) NOT NULL,
  `level` varchar(20) NOT NULL,
  `created_at` varchar(100) NOT NULL,
  `updated_at` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `username`, `password`, `fullname`, `is_active`, `level`, `created_at`, `updated_at`) VALUES
(1, 'wicak2314', 'qwertyuiop', 'Muhamad Nur Wicaksono', 'off', 'user', '', '2023-02-24 08:07:56'),
(5, 'wicakmn', '12345', 'WicakAdmin', 'off', 'admin', '', '2023-02-24 08:50:05'),
(6, 'wicak', '12345', 'Alfiana', 'off', 'pelanggan', '', '2023-02-24 08:50:17'),
(7, 'aku', '12345', 'WicakAdmin', 'on', 'user', '', ''),
(9, 'alvi', '1234567890', 'Alfiana', 'on', 'user', '2023-02-24 08:28:16', '2023-02-24 08:50:30');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tb_jenis_obat`
--
ALTER TABLE `tb_jenis_obat`
  ADD PRIMARY KEY (`id_jenis_obat`);

--
-- Indeks untuk tabel `tb_obat`
--
ALTER TABLE `tb_obat`
  ADD PRIMARY KEY (`id_obat`),
  ADD KEY `id_jenis_obat` (`id_jenis_obat`);

--
-- Indeks untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tb_jenis_obat`
--
ALTER TABLE `tb_jenis_obat`
  MODIFY `id_jenis_obat` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT untuk tabel `tb_obat`
--
ALTER TABLE `tb_obat`
  MODIFY `id_obat` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `tb_obat`
--
ALTER TABLE `tb_obat`
  ADD CONSTRAINT `tb_obat_ibfk_1` FOREIGN KEY (`id_jenis_obat`) REFERENCES `tb_jenis_obat` (`id_jenis_obat`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
